import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(name="/HelloWorld",urlPatterns={"/Hello","/HelloWorld"},loadOnStartup=1, description="Hello World")
public class HelloWorld extends HttpServlet {
    private String username = "";

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print("<form name=\"loginForm\" method=\"post\" action=\"HelloWorld\">\n" +
                "    Username: <input type=\"text\" name=\"username\"/> <br/>\n" +
                "    <input type=\"submit\" />\n" +
                "</form>");


        if(!username.equals("")){
            out.println("<HTML>");
            out.println("<HEAD><TITLE>Hello " + username + "</TITLE></HEAD>");
            out.println("<BODY>");
            out.println("Hello " + username);
            out.println("</BODY></HTML>");
            out.close();
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        username = request.getParameter("username");
        response.sendRedirect("HelloWorld");
    }
}